\documentclass[11pt,spanish,a4paper,twocolumn]{article}
\input{introFCEyN}

\begin{document}

\title{Circuito RC}
\author{}
\date{}
\maketitle


Un capacitor está constituido por dos placas conductoras separadas por una distancia pequeña (respecto de las longitudes características de las placas). Generalmente, entre ellas hay un medio dieléctrico. Si se conecta el capacitor a una fuente, las cargas se distribuyen en las superficies, llegando a un equilibrio como se muestra en la figura \ref{fig:1}.
En cada placa, hay igual cantidad de carga pero de signo contrario. La diferencia de potencial $\Delta V$ (usualmente solo \(V\)) que existe entre las dos placas conductoras es proporcional a la carga $Q$ que hay en cada placa (medido en \textit{Coulomb}). Esto se expresa de la forma:

\begin{equation}\label{rc}
	Q = C \times \Delta V
\end{equation}
donde \(C\) es la constante de proporcionalidad llamada \textit{capacitancia}.
Esta constante depende de las características del capacitor (la superficie de las placas, la distancia de separación y el material entre las mismas). La unidad de la capacitancia es el \textit{faradio} (ecuación \ref{unidades}).

\begin{equation}\label{unidades}
	Faradio = Coulomb / Volt
\end{equation} 

\begin{figure}[ht]
\centering
\includegraphics[width=6cm]{./placas_paralelas.jpg} \caption{Esquema de capacitor de placas paralelas.} \label{fig:1}
\end{figure}

Para estudiar las propiedades de un capacitor, se puede armar el circuito que se muestra en la figura \ref{fig2}. 

\begin{figure}[ht]
\centering
\includegraphics[width=5cm]{./circuito_rc_con_switch.jpg} \caption{Circuito RC. Dos posibles configuraciones: (1) conectado a la fuente de tensión, y (2) al desconectar la fuente. $V_{R}$ y $V_{C}$ indican las mediciones de diferencia de potencial sobre cada uno de los elementos.} \label{fig2}
\end{figure}

Cuando la llave conecta a la fuente (en posición (1), figura \ref{fig2}) ecuación de mallas para el circuito puede escribirse de la siguiente forma:

\begin{equation}\label{ec3}
V_{0} = V_{C} +V_{R}
\end{equation} 

donde $V_{C}$ es la diferencia de potencial sobre el capacitor y $V_{R}$ es la diferencia de potencial sobre la resistencia. Reemplazando $V_{C}$ con la ecuación {rc} y $V_{R}$ con la Ley de Ohm se obtiene:

\begin{equation}\label{ec4}
V_{0}=\frac{Q}{C}+R \cdot I
\end{equation} 

Teniendo en cuenta que la corriente $I$ es el caudal de carga, se puede escribir la siguiente ecuación diferencial ordinaria para la carga,

\begin{equation}\label{ec5}
V_{0}=\frac{Q}{C}+R \cdot \frac{dQ}{dt}
\end{equation} 

Para hallar la solución a esta ecuación se plantea por un lado la solución particular:

\begin{equation}\label{sc_particular}
Q_{p}=C \cdot V_{0}
\end{equation} 

Y la solución homogénea:

\begin{equation}\label{sc_homogenea}
Q_{h}(t)=A \cdot exp(-\frac{t}{RC})
\end{equation} 

Entonces,

\begin{equation}\label{solucion}
Q(t) = Q_{p} + Q_{h}(t) = C \cdot V_{0} + A \cdot exp(-\frac{t}{\tau})
\end{equation} 

Donde $\tau = R \cdot C$ se denomina el \textit{tiempo característico} del circuito RC. Tomando como condición inicial que la llave se hallaba en posición (2) (figura \ref{fig2}), o sea $Q(t=0)=0$ ("capacitor descargado"), se obtiene:

\begin{equation}\label{solucion_con_ci}
Q(t) = C \cdot V_{0} \cdot (1 - exp(-\frac{t}{\tau}) )
\end{equation} 

Utilizando nuevamente la ecuación \ref{rc} obtenemos una expresión para la diferencia de potencial sobre el capacitor en función del tiempo, durante la carga del capacitor:

\begin{equation}\label{VC}
V_{C}(t) = V_{0} \cdot (1 - exp(-\frac{t}{\tau}) )
\end{equation} 

Por otra parte, reemplazando esta expresión para $V_{C}$ en la ecuación \ref{ec3} se puede obtener una expresión también para $V_{R}$, durante la carga del capacitor:

\begin{equation}\label{VR}
V_{R}(t) = V_{0} - V_{C} = V_{0} \cdot exp(-\frac{t}{\tau})
\end{equation} 

Estas ecuaciones representan la evolución temporal de la diferencia de potencial sobre la resistencia y el capacitor cuando el capacitor estaba inicialmente descargado, se lo conecta a la fuente y se carga \footnote{\underline{Tarea para el hogar:} Resolver el caso en el que el capacitor estaba originalmente cargado y se descarga. \\Ayuda: La ecuación \ref{ec3} se escribe $0=V_{C}+V_{R}$ porque la fuente no esta conectada y la condición inicial es $Q(t=0)=C \cdot V_{0}$ porque la fuente esta inicialmente conectada.}

Este circuito tiene particular importancia en biología porque se utiliza para modelar la membrana celular (Ver Apéndice).

Es importante destacar que si bien las soluciones halladas dependen del tiempo, tienen un comportamiento tal que a tiempos largos llegan a un equilibrio. Ese equilibrio se denomina \textit{estado estacionario}, mientras que el tiempo que transcurre hasta alcanzar ese estado, se denomina \textit{estado transitorio}. En la práctica anterior, donde se estudió la Ley de Ohm, se midió la corriente y la diferencia de potencial en un circuito en estado estacionario, es decir que no había que preocuparse por lo que pasaba cuando se cerraba una llave sino que se asumía que por el circuito instantáneamente circula una corriente igual a la medida o simplemente que se medía lejos del momento en que se encendía la llave (a tiempos en los cuales el sistema se encontraba en un estado
estacionario).
Ahora se espera ver que pasa a tiempos cortos o cercanos al momento en que se enciende o se apaga una llave, intentando estudiar como varía la carga en función del tiempo ($Q(t)$) en el capacitor durante los procesos de carga y de descarga del mismo. ¿Qué son tiempos cortos y tiempos largos? ¿Cuáles son las condiciones del circuito en estado estacionario? 

El objetivo de la presente práctica es entonces estudiar el comportamiento no estacionario (transitorio) de un circuito compuesto por un capacitor y una resistencia; y determinar la constante $\tau$ del circuito.


\section{Actividades}
Para estudiar la dinámica del circuito se propone armar el dispositivo de la figura \ref{fig2} para estudiar las transiciones de la configuración 1 a la 2 y viceversa; es decir, conectando y desconectando el circuito a la batería.

\subsection{Fuente}
Para no tener que utilizar una llave manual para cambiar de configuración, que además puede introducir ruido, se puede utilizar un generador de ondas con perfil cuadrado de base cero y voltaje máximo (con eso imita la llave). 

Elija una frecuencia para la onda de manera tal que vea todo el comportamiento de carga y descarga del circuito. Cuando se emplea una onda cuadrada la fuente nos entrega una tensión fija $V_{0}$ (equivale a la conexión (1) de figura \ref{fig2}) durante un intervalo de tiempo relacionado con la frecuencia seleccionada, y en el intervalo de tiempo siguiente, entrega una tensión aproximadamente. nula (equivale a la conexión (2) de figura \ref{fig2}). Entonces, el intervalo en que la onda cuadrada esta en un estado debe ser suficientemente largo como para permitir llegar al estacionario antes de cambiar de configuración.

\subsection{Mediciones}
A diferencia de la práctica de Ley de Ohm, en este caso se esperan obtener mediciones continuas. Para ello se utilizará el SensorDAQ con dos puntas sobre nodos del circuitos. 

Al utilizar el generador de funciones, una de las puntas está conectada a la \textit{tierra} (la tercer pata de cualquier enchufe), que es la misma que la \textit{tierra} del SensorDAQ.
Es decir, que ambas puntas (del generador y del SensorDAQ) están conectadas por fuera del circuito, por lo que si se conectan a distintos nodos una parte del circuito quedará aislada ("cortocircuitada" por la tierra).
Discutir con el docente la función de la referencia de tierra en el laboratorio.
¿Qué significa ``conectar a tierra''?
¿A dónde va la ``tercera pata'' del enchufe?


\subsection{Medidas}
Realizar la experiencia para un $R$ y $C$ dado, midiendo la carga y la descarga, y determinando a partir de ajustes de las curvas\footnote{Linealizando y realizando un ajuste lineal.} la constante $\tau$. A su vez, determinar este valor de forma independiente a través de mediciones directas de $R$ y $C$ con el multímetro y comparar. Repetir la experiencia para otros valores de $R$ y $C$. 

\textbf{IMPORTANTE! Una conexión incorrecta puede causar cortocircuitos involuntarios y dañar el instrumental. Verifique la conexión del circuito con el docente antes de encender la fuente de tensión.}


\section{Apéndices}
\subsection{Analogía del circuito RC con membranas biológicas}
En base a circuitos eléctricos con los elementos que si vieron en la práctica anterior -como resistencias y diodos- se pueden modelar una gran cantidad de fenómenos relacionados a sistemas de irrigación. Pero el caso del circuito RC es paradigmático en cuanto a la aplicación de un circuito (y una analogía) sencillo para modelar un fenómeno biológico. El fenómeno en cuestión es la membrana celular y le valió el premio nobel a John Eccles, Alan L. Hodgkin y Andrew F. Huxley en 1963. En su trabajo desarrollaron los mecanismos iónicos de la excitación e inhibición sináptica y conducción señales a través del axón, lo cual sentó las bases de la neurofisiología actual.

En primer lugar, debe considerarse a la bicapa lipídica como un capacitor que separa a las cargas diluidas en el medio externo e interno de la célula, por un medio dieléctrico (lípidos). Luego, los canales iónicos que la atraviesan funcionan como conductores con una resistencia (o permeabilidad) específica para un ion. Y a su vez, existen fuentes que representan el potencial de equilibrio de Nerst de cada ion. Esto puede verse la figura \ref{Eccles_fig3} tomada de la presentación del Nobel de Eccles \cite{Eccles63}.

\begin{figure}[ht]
\centering
\includegraphics[width=0.5\textwidth]{./Eccles_NobelLecture_1963_Fig3.jpg} \caption{Circuito equivalente de membrana para motoneuronas de gato. (A) Concentraciones intra- y extra-celulares aproximadas para los distintos iones y sus correspondientes potenciales de equilibrio, (B) Circuito equivalente de membrana, y (C) Diagrama de los flujos a través de la membrana. %Figura tomada de \citep{Eccles63}
} \label{Eccles_fig3}
\end{figure}
Este modelo puede extenderse considerando no sólo todos los canales al mismo tiempo (en vez de una sola resistencia equivalente), si no lo que se llaman canales activos que dan lugar al potencial de acción como se muestra en la figura \ref{Huxley_fig6} (simbolizados con resistencias cruzadas por una flecha) en un modelo no mucho más complejo \cite{Huxley63}.

\begin{figure}[ht]
	\centering
	\includegraphics[width=.5\textwidth]{./Huxley_NobelLecture_1963_Fig6.jpg}
	\caption{Circuito equivalente de membrana para el axón gigante del calamar.} %Figura tomada de \citep{Huxley63}
	\label{Huxley_fig6}
\end{figure}

\subsection{Unidades}
\noindent
Diferencia de Potencial o Voltaje o Tensión: Volts (V)
\\Carga: Coulomb (C)
\\Intensidad de Corriente: Ampere (A) = C/seg
\\Resistencia: $\Omega$
\\Capacitancia: Faradio (F) = C/V

\begin{thebibliography}{9}
\bibitem{Eccles63}
  John Eccles,
  \emph{The Ionic Mechanism of Postsynaptic Inhibition},
  Nobel Lecture in Physiology or Medicine,
  1963
  \url{http://www.nobelprize.org/nobel_prizes/medicine/laureates/1963/eccles-lecture.pdf}
\bibitem{Huxley63}
  Andrew F. Huxley,
  \emph{The Quantitative Analysis of Excitation and Conduction in Nerve},
  Nobel Lecture in Physiology or Medicine,
  1963
  \url{http://www.nobelprize.org/nobel_prizes/medicine/laureates/1963/huxley-lecture.pdf}
\end{thebibliography}

\end{document}

\documentclass[aip,reprint,nofootinbib,floatfix]{revtex4-1} % for checking your page length

\usepackage{graphicx}

\usepackage[spanish, es-tabla]{babel} % http://minisconlatex.blogspot.com.ar/2013/03/latex-en-espanol.html
\def\spanishoptions{argentina}
\usepackage{babelbib}
\selectbiblanguage{spanish}

\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}

\usepackage[arrowdel]{physics}
\usepackage[separate-uncertainty=true, multi-part-units=single, locale=FR]{siunitx}
\usepackage{isotope} % $\isotope[A][Z]{X}\to\isotope[A-4][Z-2]{Y}+\isotope[4][2]{\alpha}


%% biblatex$
%\usepackage[style=numeric, backend=biber, sorting= none]{biblatex}
% \DefineBibliographyStrings{spanish}{}
% \usepackage{csquotes}
%\addbibresource{Bibliografia.bib}


\begin{document}

\title{Respuesta de la corriente en función de la diferencia de potencial}

%\author{D. T. W. Buckingham}
%\email[]{dbuckingham@physics.montana.edu}
%\homepage[]{Your web page}
%\thanks{}
%\altaffiliation{}
\affiliation{Laboratorio de Electromagnetismo y Óptica, Dep. de Física, FCEyN, UBA}

%\date{verano 2019}

\begin{abstract}
\noindent
\textbf{Objetivo:}
Registrar la corriente eléctrica que circula a través de una resistencia o un diodo en función de la diferencia de potencial continua que se aplica en sus diversos extremos.\\ \\\textbf{Temáticas}: potencial electrostático, corriente eléctrica, resistencia, ley de Ohm.
\end{abstract}

\maketitle %\maketitle must follow title, authors, abstract and \pacs


\section{Corriente en función de la diferencia de potencial eléctrico}
Se dice que un material es conductor cuando posee una gran cantidad de cargas libres (negativas, electrones).
Entonces, así como una cañería puede llevar cierto caudal de agua, a través de un material conductor se puede mover un ``caudal'' de electrones, que llamaremos \textbf{corriente} (\(\mathrm{I}\)).
Para que el agua circule por las cañerías de una casa es necesario aplicar cierta diferencia de potencial gravitatorio, por ejemplo poniendo el tanque arriba de la casa; de la misma forma, para que los electrones circulen es necesario aplicar cierta \textbf{diferencia de potencial eléctrico} (\(\Delta \mathrm{V}\) usualmente abreviado a \(V\), informalmente llamada \emph{tensión} o \emph{voltaje}).
En general, la analogía con las cañerías es buena para pensar a los circuitos eléctricos mientras no se tengan otras herramientas, aunque como toda analogía es limitada.
De hecho los circuitos eléctricos son utilizados para modelar una gran cantidad de flujos (como el sistema circulatorio o el sistema de irrigación en plantas o tierras).
La \(I\), como el caudal, puede ser positiva o negativa según el sistema de referencia que se tome y el sentido de circulación de las cargas, que por convención se consideran positivas.
La \(\Delta V\) también puede cambiar de signo según el sistema de referencia\footnote{Para pensar: ¿Cuándo y cómo definen el sistema de referencia?}.

Ante el paso de una \(I\) o la aplicación de una \(\Delta V\) entre dos puntos la relación entre ambas cantidades puede variar de un material o combinación de materiales a otro.
Un caso particular de estas relaciones es la \emph{Ley de Ohm} (ecuación \ref{eq:ohm}), que junto con la \emph{Ley de Hooke} para los resortes o los materiales elásticos, es probablemente una de las leyes experimentales más utilizadas, y plantea justamente una relación lineal 
\begin{equation}
	\Delta V = I R,
	\label{eq:ohm}
\end{equation} 
donde \(R\) se denomina resistencia y depende del material.
En general, la validez de esta ley depende fuertemente del material, es así que hay materiales que se llaman óhmicos o no óhmicos, según si siguen o no dicha relación.

Los materias pueden combinarse de distintas formas para formar un circuito en el que circule la \(I\), generando otras relaciones sean también óhmicas o no dependiendo de los materiales utilizados.
A lo largo de esta práctica les proponemos explorar la relación entre \(\Delta V\) y \(I\) para distintos posibles componentes de un circuito, o una combinación de ellos.


\section{Actividades}
Se quiere ilustrar la relación funcional entre \(I\) y \(\Delta V\), generando la llamada \emph{curva I-V}.
Para ello, realizar una serie de mediciones (e.g. 10) que barran un rango amplio de \(\Delta V\), desde valores muy negativos hasta valores muy positivos.
Luego, seleccionar la/las regiones que les resulten más interesantes y agregar más puntos a fin de poder describir mejor el comportamiento del sistema en esas regiones.

\textbf{Para pensar:} ¿Cómo lograr valores negativos de voltaje?
¿Qué ocurre si se conectan los cables a las distintas salidas de la fuente (-9V, TIERRA, +9V, VARIABLE)(ver figura \ref{fig:fuente})?

Las fuentes de voltaje existentes en el laboratorio pueden funcionar tanto en un régimen continuo como en alterna (figura \ref{fig:fuente}).
Para esta práctica solo se utilizará el modo continuo.
Con la perilla que aparece en la figura \ref{fig:fuente} es posible regular la tensión que entrega la fuente.
\begin{figure}[ht]
	\centering
	\includegraphics[width=6cm]{./fuente_continua.jpg}
	\caption{Ejemplo de fuente de voltaje.}
	\label{fig:fuente}
\end{figure}


\subsection{Resistencias}
Comenzar trabajando con un circuito compuesto por una fuente y una resistencia
¿Sigue esta la Ley de Ohm propuesta?
¿Esta le vale en todo el rango de voltajes y corrientes que midieron?
Para poner a prueba la ley se sugiere ajustar una función lineal a los puntos medidos, obtener de la pendiente el valor de la resistencia.
Se sugiere probar distintas resistencias (entre 5K$\Omega$ y 51K$\Omega$).
¿Qué sucede si se intercambian los cables conectados a la fuente mientras se está midiendo?


\subsection{Diodos}
Repetir el procedimiento seguido con la resistencia pero para un diodo (se sugiere el 4148).
¿Identifica un modelo (una ecuación) que represente la relación \(I\) vs. \(\Delta V\)?
¿Este modelo vale en todo el rango de voltajes y corrientes medidas?

\textbf{Precaución:} No debe cortocircuitarse la fuente, por lo que si creemos que esto puede ocurrir o no sabemos que es lo que va a ocurrir, debemos colocar una resistencia en serie con la fuente (¿por qué?).


\subsection{Combinaciones de resistencias}
Probar distintas configuraciones de resistencias (en serie o paralelo), utilizando distintos valores.
Comparar los resultados obtenidos con las predicciones teóricas para cada configuración.

Finalmente reproduzca alguno de los circuitos del ejercicio 2 de la segunda guía de trabajos prácticos y contraste la corriente que calculó con la que mide.


\section{Modos del multímetro}
El multímetro sirve para medir tanto corriente como voltaje (figura \ref{fig:multimetro}).

\begin{figure}[ht]
	\centering
	\includegraphics[width=0.5\columnwidth]{./voltimetro.jpg}
	\caption{Ejemplo de multímetro}
	\label{fig:multimetro}
\end{figure}

En cada caso es necesario seleccionar el modo correcto (preguntar al docente), seleccionar la escala (pensar en cada caso qué escala utilizar y en base a eso cuál es el error de cada medición) y conectar correctamente los cables según vayan a medir corriente o voltaje. La figura \ref{fig:configuraciones} ilustra como conectar el multímetro para realizar mediciones de:
\begin{itemize}
\item \textit{Voltaje (voltímetro):} mide \(\Delta V\) entre los puntos A y B del circuito, que no necesariamente deben ser adyacentes ni cercanos, conectando el multímetro \emph{en paralelo}.
\item \textit{Corriente (amperímetro):} mide la corriente que pasa por una rama del circuito, conectando el multímetro \emph{en serie}.
\end{itemize}

\begin{figure}[ht]
	\centering
	\includegraphics[width=\columnwidth]{./configuraciones.jpg}
	\caption{Configuraciones para conectar el multímetro como: Izquierda: Voltímetro, y Derecha: Amperímetro}
	\label{fig:configuraciones}
\end{figure}



\section{Unidades}
\noindent
Diferencia de Potencial o Voltaje o Tensión: Volts (V)
\\Carga: Coulomb (C)
\\Intensidad de Corriente: Ampère (A) = C/s
\\Resistencia: Ohm ($\Omega$)


% \nocite{Alonso1998,Purcell1988,Reitz1996,Trelles1984}
% \bibliographystyle{unsrt} 
% \bibliography{Bibliografia}




\end{document}

\documentclass[aip,reprint,nofootinbib,floatfix]{revtex4-1} % for checking your page length

\usepackage{graphicx}

\usepackage[spanish, es-tabla]{babel} % http://minisconlatex.blogspot.com.ar/2013/03/latex-en-espanol.html
\def\spanishoptions{argentina}
\usepackage{babelbib}
\selectbiblanguage{spanish}

\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}

\usepackage[arrowdel]{physics}
\usepackage[separate-uncertainty=true, multi-part-units=single, locale=FR]{siunitx}
\usepackage{isotope} % $\isotope[A][Z]{X}\to\isotope[A-4][Z-2]{Y}+\isotope[4][2]{\alpha}


%% biblatex$
%\usepackage[style=numeric, backend=biber, sorting= none]{biblatex}
% \DefineBibliographyStrings{spanish}{}
% \usepackage{csquotes}
%\addbibresource{Bibliografia.bib}


\begin{document}

\title{Electrostática: mapeando un campo eléctrico}

%\author{D. T. W. Buckingham}
%\email[]{dbuckingham@physics.montana.edu}
%\homepage[]{Your web page}
%\thanks{}
%\altaffiliation{}
\affiliation{Laboratorio de Electromagnetismo y Óptica, Dep. de Física, FCEyN, UBA}

%\date{verano 2019}

\begin{abstract}
\noindent
\textbf{Objetivo:}
% Abstract goes here.
  Dibujar mapas de líneas o superficies equipotenciales, es decir, del conjunto de puntos en el espacio que se encuentran caracterizados por un mismo valor del potencial eléctrico \(V\) para distintas configuraciones de electrodos conectados a una fuente de baja tensión e inmersos en un medio líquido poco conductor.\\ \\
  \textbf{Temáticas}: pcampo eléctrico, potencial electrostático, conductores y dieléctricos.
\end{abstract}

\maketitle %\maketitle must follow title, authors, abstract and \pacs


\section{Campo y potencial eléctrico}
Toda fuerza aplicada a lo largo de una trayectoria entre los puntos en el \(\va{r}_1\) y \(\va{r}_2\) está haciendo un trabajo
\begin{equation} 
  W_{12}= \int_1^2 \va{F} \va{\dd{r}},
\end{equation}
y todo trabajo de una fuerza conservativa se corresponde con el negativo de una variación de una energía potencial.
Un ejemplo de esto es el de la fuerza peso \(\va{P}= m \va{g}\), en cuyo caso la energía potencial es la gravitatoria
% y todo trabajo de una fuerza conservativa, como el la eléctrica, se corresponde con el negativo de una variación de una energía potencial, en este caso eléctrica
\begin{equation} 
  W_{peso\,12}= \int_1^2 m\va{g} \va{\dd{r}}= - \Delta E_\text{pot. grav.}= m g (h_2 - h_1), 
  % W_{12}= - \Delta E_\text{eléctrica}. 
  \label{travail}
\end{equation}
donde \(h_2\) y \(h_1\) son dos alturas siendo la primera mayor, siempre y cuando consideremos que el campo gravitatorio \(\va{g}\) no varia entre esas dos alturas.

Ahora presentaremos otras fuerza conservativa, la electrostática.
En lugar de \(m\) ubicaremos una carga de prueba \(q\), y en lugar del campo gravitatorio \(\va{g}\) tendremos un campo campo eléctrico \(\va{E}\).
Este último ejerce sobre \(q\) una fuerza
\begin{equation} 
  \va{F} = q \: \va{E}.
  \label{forza}
\end{equation}
Como la fuerza \(\va{F}\) es un vector y la carga eléctrica \(q\) un escalar, resulta
claro que el campo eléctrico local \(\vec{E}\) es también un vector. 

El trabajo de esta fuerza se calcula igual que en (\ref{travail})
\begin{equation} 
  W_{elec.\,12}= \int_1^2 q \va{E} \va{\dd{r}}= - \Delta E_\text{elec.}.
  \label{travailElect}
\end{equation}
Es útil independizarse de la magnitud de la carga de prueba \(q\) para describir la variación de la energía, por lo que se acostumbra definir
\begin{equation} 
  \frac{W_{elec.\,12}}{q}= \int_1^2 \va{E} \va{\dd{r}}= - \Delta V,
  \label{travailElect2}
\end{equation}
donde \(V\) recibe el nombre de potencial eléctrico.
Y así como $W_{12}$ el trabajo que tenemos que realizar para llevar la carga $q$ desde el punto 1 al punto 2 $W_{12}$ es una magnitud escalar, el potencial también lo es.

Así como podemos invertir la relación (\ref{travail}) y puede obtenerse \(\va{P}\) como el gradiente de la \(E_\text{gravitatoria}\), es decir \(\va{P}= - \grad E_\text{gravitatoria}\), esto mismo se aplica a cualquier fuerza conservativa, como la electrostática
\begin{equation} 
  \va{F}_\text{electrostática}= - \grad E_\text{electrostática} = - q \grad V.
\end{equation}
Y como vimos en la expresión (\ref{forza}) la relación de esta fuerza con \(\va{E}\) es claro que las componentes del campo eléctrico pueden expresarse en función del potencial eléctrico:
\begin{equation}
    \vec{E} = - \vec{\nabla} V.
\end{equation}

Y así como si nos movíamos perpendiculares a \(\va{g}\) la componente de \(\va{P}\) en esa dirección era nula, es decir si manteníamos a la misma altura manteníamos la misma \(E_\text{pot. grav.}\), si nos movemos un una línea donde el potencial eléctrico es constante, $\Delta V=0$ (línea equipotencial), la componente de $\va{E}$ sobre esta línea es nula.
En otras palabras, $\va{E}$ es siempre perpendicular a las líneas (o, más generalmente, superficies) equipotenciales.

La idea central de este experimento consiste en determinar para una dada configuración las líneas equipotenciales (es decir, las líneas sobre las cuales el potencial es constante).
A partir de estas líneas equipotenciales, se pueden luego hallar las líneas de campo $\va{E}$, trazando líneas perpendiculares a las equipotenciales.


\section{Dispositivo experimental}
Una bandeja de vidrio o acrílico transparente, de aproximadamente \(30 \times 20 \times \SI{4}{\centi\metre}\) contendrá la mínima cantidad de agua de red necesaria para cubrir toda la base de la bandeja. 
La base de la bandeja rectangular tiene un papel milimetrado que permite conocer las coordenadas de cada punto \(P(x,y)\).  
Dos placas metálicas conductoras se dispondrán parcialmente inmersas en el líquido (ver figura \ref{fig:1}).
Dos cables banana-cocodrilo conectarán las placas a una fuente que impondrá una diferencia de potencial constante entre ambas.
% Dos cables banana-cocodrilo conectarán las placas a una fuente de tensión alterna de \num{5}-\SI{15}{\volt}, que impondrá una diferencia de potencial entre ambas.\footnote{Se utiliza alterna con el fin de minimizar la degradación por electrólisis de los metales.}
Para determinar el valor del potencial en un punto dado del espacio, se hará uso de un voltímetro.
Un cable banana-cocodrilo conectará el puerto común del multímetro a una de las placas y un cable de prueba (banana-punta) se conectará al puerto marcado con ``V''.

\begin{figure}[htb]
	\centering
	\includegraphics[width=8.5cm]{P1c}
		\caption{
			Esquema del dispositivo experimental propuesto (bandeja en vista superior).
			La bandeja de material aislante contiene agua.
			Las líneas A y B representan los electrodos metálicos.
			En el punto de coordenadas \(P(x,y)\), se mide el valor del potencial eléctrico \(V(x,y)\) respecto al del electrodo B.
			El objeto circular es alternativamente un conductor o aislante con los que pueden obtenerse configuraciones alternativas del campo eléctrico.
		}
		\label{fig:1}
\end{figure}

\section{Determinacíon de las líneas de campo}
En esta práctica se propone observar experimentalmente la formación de líneas equipotenciales para diversas distribuciones de carga (electrodos).
A partir de las mediciones, realice un análisis semi-cuantitativo para establecer las características generales que poseen las líneas de campo y las líneas equipotenciales para las configuraciones de electrodos estudiadas.

Antes de continuar y para explorar cómo se comporta el sistema use el multímetro en modo voltímetro en una escala acorde al ajuste de la fuente y ``pinche'' con la punta de prueba en diversos puntos de la bandeja.
Después de verificar que la indicación del multímetro es coherente con sus expectativas (¿donde el potencial es mayor?) podrá determinar las líneas equipotenciales en toda la bandeja siguiendo el procedimiento que se indica a continuación:
	\begin{enumerate}
		\item Mida el potencial eléctrico en puntos equiespaciados en la superficie de la bandeja, e.g. cada \SI{2}{\centi\metre}.
		\item En una planilla de cálculo anote las mediciones en la fila y columna correspondiente al punto en el sistema de coordenadas elegido.
		\item Para el análisis hará uso de un notebook de Python que recupera los datos de la planilla de cálculo y con la función \verb'contourf' traza líneas uniendo los valores iguales entre los que interpola de los datos de la matriz.
			En el caso de las mediciones de potencial \(V\) tales líneas corresponden a las equipotenciales.
	\end{enumerate}


\subsection{Efecto de conductores y aislantes}

Ahora sin mover los electrodos, coloque un conductor entre ellos.
Determine las líneas equipotenciales de este nuevo arreglo.
En particular interesa estudiar la forma de las líneas equipotenciales alrededor del conductor, por lo que no dude en aumentar la ``densidad'' de puntos medidos a su alrededor.
¿Cómo deberían ser las líneas equipotenciales dentro del mismo? No solo lo imagine, ¡mídalo y grafíquelo!

Analice la configuración de líneas equipotenciales alrededor del conductor.
¿Cómo son las líneas de campo sobre la superficie del conductor?
Tenga la precaución de utilizar otro nombre para la matriz en el programa para no sobre escribir los datos de la medición anterior.

Repita lo anterior pero usando un aislante en vez del conductor.
Estudie las líneas equipotenciales alrededor del aislante.
¿Cómo son las líneas de campo y el campo mismo sobre su superficie?
¿Cómo puede explicar lo que observa en este caso?

%\begin{figure}[ht]
%  \centering
%	\includegraphics[width=8.5cm]{P1c}
%	%\includegraphics[width=5cm]{LG01--001.png}
%    \caption{Variación del dispositivo experimental disponiendo entre los electrodos cuales conductores o dieléctricos de formas geométricas simples.
%    }
%    \label{fig:2}
%\end{figure}


\section{Unidades SI}
\noindent
Potencial eléctrico (informalmente Voltaje o Tensión): Volts (V)
\\Carga: Coulomb (C)
\\Campo eléctrico: Volt por metro (V/m)


\nocite{Alonso1998,Purcell1988,Reitz1996,Trelles1984}
\bibliographystyle{unsrt} 
\bibliography{Bibliografia}




\end{document}

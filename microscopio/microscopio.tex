\documentclass[aip,reprint,nofootinbib,floatfix]{revtex4-1} % for checking your page length

\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}

\usepackage[spanish, es-tabla]{babel} % http://minisconlatex.blogspot.com.ar/2013/03/latex-en-espanol.html
\def\spanishoptions{argentina}
\usepackage{babelbib}
\selectbiblanguage{spanish}

\usepackage{graphicx}

\usepackage[arrowdel]{physics}
\usepackage[separate-uncertainty=true, multi-part-units=single, locale=FR]{siunitx}
\usepackage{isotope} % $\isotope[A][Z]{X}\to\isotope[A-4][Z-2]{Y}+\isotope[4][2]{\alpha}

\DeclareSIUnit\oersted{Oe}
\DeclareSIUnit\gauss{G}

\usepackage{tikz}
\usepackage[siunitx]{circuitikz}
% circuitikz: rotate instruments - https://tex.stackexchange.com/questions/105864/rotate-voltmeter-circuitikz
\newcommand{\mymeter}[2] 
{  % #1 = name , #2 = rotation angle
\begin{scope}[transform shape,rotate=#2]
\draw[thick] (#1)node(){$\mathbf V$} circle (11pt);
\draw[rotate=45,-latex] (#1)  +(-17pt,0) --+(17pt,0);
\end{scope}
}


\usepackage{url}

%% biblatex$
%\usepackage[style=numeric, backend=biber, sorting= none]{biblatex}
% \DefineBibliographyStrings{spanish}{}
% \usepackage{csquotes}
%\addbibresource{Bibliografia.bib}


\begin{document}

\title{Microscopio}

%\author{D. T. W. Buckingham}
%\email[]{dbuckingham@physics.montana.edu}
%\homepage[]{Your web page}
%\thanks{}
%\altaffiliation{}
\affiliation{Laboratorio de Electromagnetismo y Óptica, Dep. de Física, FCEyN, UBA}

%\date{verano 2019}

\begin{abstract}
\noindent
\textbf{Objetivo:} Tener un primer contacto con elementos sencillos de óptica, su manipulación y su montaje.
Construir y caracterizar un microscopio compuesto.
\\ \\
% Abstract goes here.
  \textbf{Temáticas}: óptica geométrica, lentes
\end{abstract}

\maketitle %\maketitle must follow title, authors, abstract and \pacs

El microscopio más simple consta de dos lentes, una llamada objetivo por ser la más cercana al objeto a observar y otra llamada ocular.
El objetivo forma una imagen real y ampliada del objeto con la cual el ocular forma una nueva imagen virtual más ampliada que es observada por el ojo.
De esta manera se alcanzan aumentos muy superiores a los que se pueden obtener con una lupa que hace uso de una única lente.


\section{Única lente: lupa}
\begin{enumerate}
	\item Observe a través de la lente un objeto y describí cualitativamente todo lo que puedas decir de la imagen.
	Observe especialmente el tamaño y la orientación de la imagen en distintas condiciones.
	Para esto utilice como objeto la máscara con una forma de cruz calada que permite ser que ser iluminada desde atrás con una lámpara.
	\item Para distintas posiciones del objeto, determine la posición y el tamaño de la imagen.
	¿Puede decir que la imagen está aumentada?
	Determine cuantitativamente tal aumento.
	\item La expresión de Gauss (ecuación \ref{eq:Gauss}) relaciona las distancias de la lente al objeto \(p\) y a la imagen que esta forma \(q\) (ver figura \ref{fig:lente}),
	\begin{equation}
		\frac{1}{p}+ \frac{1}{q} = \frac{1}{f},
		\label{eq:Gauss}
	\end{equation}
	donde \(f\) es la distancia focal de la lente.
	\item ¿Con qué error se determina la distancia focal de la lente usando este método?
	\begin{figure}[h]
		\centering
		\includegraphics[width=\columnwidth]{ab-000.png}
		\caption{Esquema del dispositivo con una lente.
		Definición de las distancias relevantes y método de trazado de rayos.}
		\label{fig:lente}
	\end{figure}
\end{enumerate}


\section{Ocular y objetivo: microscopio compuesto}
Para la construcción de un elemental microscopio compuesto se utilizan dos lentes, una de corta distancia focal que será el objetivo (\(f_{ob} \simeq \SI{5}{\centi\metre}\)) y otra de mayor que será el ocular (\(f_{oc} \simeq \SI{10}{\centi\metre}\)).
A fin de independizar el aumento de la posición del ojo del observador, se enfoca el microscopio al infinito de modo que los rayos de luz que salen del ocular sean paralelos.
Para lograr esto es necesario ubicar la lente ocular de modo tal que la imagen real de la lente objetivo se localice en el foco del ocular.
El dispositivo a armar se esquematiza en la figura \ref{fig:compuesto}.
La lente objetivo y la lente ocular deberán estar bien sujetas por medio de vástagos y torretas al banco o mesa óptica.
Tome el debido tiempo para alinear y fijar todos los componentes cuidadosamente.
\begin{figure}[h]
	\centering
	\includegraphics[width=\columnwidth]{ao-000.png}
	%\includegraphics[width=\columnwidth]{ab-001.png}
	\caption{Esquema de un microscopio compuesto. Definición de las distancias relevantes.}
	\label{fig:compuesto}
\end{figure}




\subsection{Aumento}
Se define como aumento eficaz \(D\)
\begin{equation}
	D = \frac{ \tan{u'} }{ \tan{u} },
	\label{eq:eficaz}
\end{equation}
donde \(u\) es el ángulo subtendido por el objeto mirado a ojo desnudo a \SI{25}{\centi\metre} de distancia, que constituye un promedio de la distancia a la que mejor enfoca el ojo sobre objetos cercanos; y \(u'\) es el ángulo bajo el cual se ve la imagen final vista a través del ocular (ver figura \ref{fig:ocular}).
\begin{figure}[h]
	\centering
	\includegraphics[width=0.75\columnwidth]{ao-001.png}
	\caption{Ángulo subtendido por un objeto mirado a ojo desnudo a \SI{25}{\centi\metre} de distancia.}
	\label{fig:ocular}
\end{figure}
En la figura \ref{fig:compuesto} puede observarse que 
\begin{equation}
	\tan{u'} = \frac{ y' }{ f_{oc} },
	\label{eq:tangentau}
\end{equation}
donde \(y'\) es la posición de la imagen de la lente objetivo y \(f_{oc}\) la distancia focal de la ocular.
Por semejanza de los triángulos que aparecen sombreados en la figura \ref{fig:compuesto} se observa que
\begin{equation}
	\frac{ y' }{ y } = \frac{ \delta }{ f_{ob} },
	\label{eq:semejantes1}
\end{equation}
donde \(y\) es la posición del objeto observado, \(\delta\) (delta) es la distancia que hay entre el foco imagen del objetivo y la posición donde se forma la imagen, y \(f_{ob}\) es la distancia focal de la lente objetivo.
Despejando \(y'\) de la expresión \ref{eq:semejantes1} y reemplazando por lo despejado en \ref{eq:tangentau} se obtiene
\begin{equation}
	\tan{u'} = \frac{y }{ f_{oc} f_{ob} } \times \delta.
	\label{eq:tanup}
\end{equation}
A su vez de la figura \ref{fig:ocular} es evidente que
\begin{equation}
	\tan{u} = \frac{y}{ \SI{25}{\centi\metre} } .
	\label{eq:tanu}
\end{equation}
Reemplazando las expresiones \ref{eq:tanu} y \ref{eq:tanup} en la \ref{eq:eficaz} se obtiene el aumento del microscopio expresado en función de longitudes fácilmente medibles
\begin{equation}
	D = \frac{ \SI{25}{\centi\metre} }{ f_{ab}  f_{oc} } \times \delta.
	\label{eq:aumento}
\end{equation}


\subsection{Medición del aumento}
Para lo siguiente se requiere de dos pantallas milimetradas, el objeto calado en forma de cruz y la lámpara utilizados anteriormente.
\begin{enumerate}
	\item Monte la lámpara para que ilumine al objeto en forma de cruz.
	Coloque la lente objetivo y mueva la pantalla milimetrada hasta obtener una imagen nítida de la cruz.
	\item Sin olvidar de dejar marcada su posición, quitar la pantalla y ubicarla donde estaba el objeto cruz.
	\item Ubicar la lente ocular de forma que su foco coincida con la posición que ocupada la pantalla milimetrada.
	Como sabe cuanto es \(f_{oc}\) puede ahora determinar cuanto es \(\delta\) (ver figura \ref{fig:compuesto}).
	\item Observe por el ocular la imagen aumentada de la pantalla.
	\item Para medir el aumento sostenga una segunda pantalla milimetrada a \SI{25}{\centi\metre} de los ojos y simultáneamentente observe por el microscopio la otra pantalla.
	¿Cuántas divisiones de la pantalla más cercana a su ojo \(N_1\) entran en que número de divisiones de la pantalla ampliada por el microscopio \(N_2\)?
	Esto le permitirá estimar el aumento \(D'\)
	\begin{equation}
		D' \simeq \frac{N_1}{N_2}.
		\label{eq:aumentoExperimental}
	\end{equation}
	\item Compare el aumento del microscopio que determinó \(D'\) con el \(D\) que se obtiene con la ecuación \ref{eq:aumento}.
\end{enumerate}

Responda:
\begin{enumerate}
	\item ¿De qué variables depende el aumento para el microscopio construido?
	¿Cómo se determinan experimentalmente?
	\item Si quiere usar una cámara en lugar del ojo para adquirir imágenes, ¿cómo modificaría el dispositivo?
\end{enumerate}


%\section{Microscopio de laboratorio}
%B) Microscopio de Laboratorio
%En esta parte de la práctica se utilizará un microscopio de laboratorio el cual consta
%de varios objetivos y un ocular compuesto.
%El microscopio está diseñado de modo tal que la distancia entre el objeto y la
%posición en donde se forma la imagen del objetivo está estandarizada, a fin de que sea
%mínimo el ajuste necesario para mantener en foco al objeto cuando se cambia de objetivo.
%1) Calibración del micrómetro ocular
%El ocular del microscopio posee una escala que es necesario calibrar para los
%distintos aumentos que se puede lograr con el mismo. Para ello, se observa una platina que
%tiene una escala de dimensiones conocidas, es decir hay una distancia X entre las divisiones
%de la misma. Por el microscopio se observan ambas escalas y se determina el número N de
%divisiones de la platina que coinciden con n divisiones del micrómetro del ocular.
%La calibración se debe realizar para cada objetivo del microscopio.
%2) Determinación del aumento eficaz del microscopio
%Observar una platina milimetrada por el microscopio y simultáneamente otra a ojo
%desnudo ubicada a 25 cm del observador de modo de estimar los diferentes aumentos del
%mismo.
%3) Medición de un objeto.
%Usando la escala calibrada del ocular, medir un objeto para los distintos aumentos
%del microscopio.


\begin{thebibliography}{9}
\bibitem{Gil01}
Salvador Gil y E. Rodriguez,
\emph{Física re-Creativa: Experimentos de Física usando nuevas
tecnologías},
Prentice Hall, Buenos Aires (2001)
\url{www.fisicarecreativa.com}
\bibitem{Hecht98}
E. Hecht,
\emph{Óptica},
Addison Wesley, 3"a ed., Capítulo 5 (1998).
\end{thebibliography}



\end{document}

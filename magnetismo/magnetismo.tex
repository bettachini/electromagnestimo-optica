\documentclass[aip,reprint,nofootinbib,floatfix]{revtex4-1} % for checking your page length

\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}

\usepackage[spanish, es-tabla]{babel} % http://minisconlatex.blogspot.com.ar/2013/03/latex-en-espanol.html
\def\spanishoptions{argentina}
\usepackage{babelbib}
\selectbiblanguage{spanish}

\usepackage{graphicx}

\usepackage[arrowdel]{physics}
\usepackage[separate-uncertainty=true, multi-part-units=single, locale=FR]{siunitx}
\usepackage{isotope} % $\isotope[A][Z]{X}\to\isotope[A-4][Z-2]{Y}+\isotope[4][2]{\alpha}

\DeclareSIUnit\oersted{Oe}
\DeclareSIUnit\gauss{G}

\usepackage{tikz}
\usepackage[siunitx]{circuitikz}
% circuitikz: rotate instruments - https://tex.stackexchange.com/questions/105864/rotate-voltmeter-circuitikz
\newcommand{\mymeter}[2] 
{  % #1 = name , #2 = rotation angle
\begin{scope}[transform shape,rotate=#2]
\draw[thick] (#1)node(){$\mathbf V$} circle (11pt);
\draw[rotate=45,-latex] (#1)  +(-17pt,0) --+(17pt,0);
\end{scope}
}


\usepackage{hyperref}

%% biblatex$
%\usepackage[style=numeric, backend=biber, sorting= none]{biblatex}
% \DefineBibliographyStrings{spanish}{}
% \usepackage{csquotes}
%\addbibresource{Bibliografia.bib}


\begin{document}

\title{Campo magnético terrestre}

%\author{D. T. W. Buckingham}
%\email[]{dbuckingham@physics.montana.edu}
%\homepage[]{Your web page}
%\thanks{}
%\altaffiliation{}
\affiliation{Laboratorio de Electromagnetismo y Óptica, Dep. de Física, FCEyN, UBA}

%\date{verano 2019}

\begin{abstract}
\noindent
\textbf{Objetivo: Determinar cuantitativamente valor y dirección del campo magnético terrestre local% mediante dos m\'etodos experimentales diferentes.
}
\\ \\
% Abstract goes here.
  \textbf{Temáticas}: Campo magnético, magnetostática, ley de Ampère, ley de Biot-Savart, sonda Hall.
\end{abstract}

\maketitle %\maketitle must follow title, authors, abstract and \pacs


\section{Sonda Hall}
Las sondas (o puntas) Hall son dispositivos basados en el efecto Hall que da cuenta de que en conductor un campo magnético \(\va{B}\) perpendicular a una corriente \(I\) que conduzca producirá una diferencia de potencial \(\Delta V\) en la dirección perpendicular a ambos.

Las sondas disponibles en el laboratorio se conectan a un puerto USB para alimentar el amplificador de la misma.
El amplificador se utiliza para aumentar \(\Delta V\), que es del orden de los \SI{3}{\milli\volt\per\gauss}, a un rango de \SI{0}{\volt} a \SI{4}{\volt} permitiendo medirla fácilmente con un voltímetro. 

Una vez que comprenda cómo funciona la sonda, explore la respuesta de la misma variando su orientación espacial.
De esta forma, encuentre el plano de trabajo de la misma (¿Qué indica el punto blanco?).


\subsection{Calibración de la sonda Hall}
Sabiendo que el campo magnético en el interior de una bobina por la cual circula una corriente continua es uniforme, podemos emplear este campo para realizar la calibración de la sonda Hall (¿Por qué razón es necesario calibrar la sonda?).

Usando una fuente de tensión, una resistencia variable, una bobina de geometría y número de vueltas conocidas y un amperímetro, arme un circuito de modo que pueda -simultáneamente- aplicar y medir la corriente que circula por la bobina. 

Diseñe un montaje que le permita mantener la sonda Hall en el centro de la bobina.
El módulo del campo magnético en el centro de la bobina de radio $R$, longitud $L$, número de vueltas $N$, por la que circula una corriente $I$ puede
aproximarse por:
\begin{equation}
    |\va{B}| = \alpha \frac{\mu_0}{2R} N I,
    \end{equation}
siendo $\alpha$ un factor de proporcionalidad que depende de las características geométricas detalladas de la bobina, y que en nuestro caso puede aproximarse por $\alpha \approx 0.28$ (verificar en cada caso) y \(\mu_0 = \SI{4 \pi d-7}{\henry\per\metre}\) es la permeabilidad magnética del vacío.

Con estos datos, calibre la sonda Hall utilizando como patrón el campo magnético de la bobina a través de un gráfico \(|\va{B}|\) vs. \(I\).


\section{Magnitud local del campo magnético terrestre}

\subsection{Medición empleando una sonda Hall}
Explore qué ocurre cuando rota la punta en 180$^\circ$ respecto de su eje, de modo que el campo magnético atraviese la punta Hall por el extremo opuesto.
Compare con algún valor de referencia para Buenos Aires.
Puede usar la \emph{Magnetic field calculator} \url{http://geomag.nrcan.gc.ca/calc/mfcal-en.php}.


\subsection{Medición deflectando una brújula}
Ubique una brújula en el centro de la bobina empleada anteriormente.
Con la bobina sin corriente determine la dirección del campo magnético terrestre.
Asegúrese de alinear correctamente la bobina, de modo que su eje quede perpendicular a la dirección del campo magnético terrestre local.

Luego haga pasar una corriente por las espiras y determine la dependencia del ángulo que se desvía la aguja (que llamaremos $\theta$) con el campo $\va{B}$ de la bobina.
A partir de la medición del ángulo de desviación, y conociendo el campo generado por la bobina, se puede determinar el campo terrestre.
Grafique $\va{B}_\text{bobina}$ vs. $\tan \left( \theta \right)$, empleando para ello no menos de 6 puntos.
¿Qué análisis puede realizar a partir de esta gráfica?



\section{Unidades}
\noindent
Inductancia eléctrica: henry (\si{\henry} = \si{\kilo\gram\meter\squared\per\second\squared\per\ampere\squared}) 
\\Campo magnético (\(\va{B}\), SI): tesla (\si{\tesla} = \si{\kilo\gram\per\second\squared\per\ampere})
\\Campo mag. auxiliar(\(\va{H}\), SI): (\si{\ampere\per\metre})
\\Campo magnético (\(\va{B}\), cgs): gauss (\si{\gauss}) =\SI{1d-4}{\tesla}
\\Campo mag. aux. (\(\va{H}\), cgs): oersted (\si{\oersted}) \(= \frac{1\times10^3}{4 \pi}\)\si{\ampere\per\metre} 

\nocite{Alonso1998,Purcell1988,Reitz1996,Trelles1984}
\bibliographystyle{unsrt} 
\bibliography{Bibliografia}


%\begin{thebibliography}{9}
%\bibitem{Eccles63}
%  John Eccles,
%  \emph{The Ionic Mechanism of Postsynaptic Inhibition},
%  Nobel Lecture in Physiology or Medicine,
%  1963
%  \url{http://www.nobelprize.org/nobel_prizes/medicine/laureates/1963/eccles-lecture.pdf}
%\bibitem{Huxley63}
%  Andrew F. Huxley,
%  \emph{The Quantitative Analysis of Excitation and Conduction in Nerve},
%  Nobel Lecture in Physiology or Medicine,
%  1963
%  \url{http://www.nobelprize.org/nobel_prizes/medicine/laureates/1963/huxley-lecture.pdf}
%\end{thebibliography}



\end{document}
